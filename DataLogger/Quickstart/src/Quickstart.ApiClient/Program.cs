﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insero.Platform.Client;
using Insero.Platform.Messages;
using Insero.Platform2.Contracts;
using Insero.Platform2.Contracts.MeasurePoints;
using Newtonsoft.Json;

namespace Quickstart.ApiClient
{
   class Program
   {
      public static void Main( string[] args ) => MainAsync().GetAwaiter().GetResult();

      public async static Task MainAsync()
      {
         var client = new Platform2Client( "https://aarhuswebapp.azurewebsites.net/services", "https://aarhuswebapp.azurewebsites.net/connect/token" );
         var context = new Platform2Context( "aarhus-client", "gmc67FHjRmsz4KhqEK27mE2vuk7D4arV", "api.nodes api.measurements" );
         client.DefaultContext = context;

         // find a node by id
         var testFolder = await client.FindNodeAsync( 1 );
         Console.WriteLine( "Folder: " + JsonConvert.SerializeObject( testFolder, Formatting.Indented ) );
         Console.WriteLine( "---" );
         
         // searching for nodes
         var form = new NodesSearchForm
         {
            Page = 0,
            PageSize = 10,                  // ensure we do not return everything
            Type = "model.node.measurePoint.custom",  // what type of nodes are we looking for
            SearchAll = true                          // specifies we want to search among all nodes; default is to search based on the node hierachy, which means looking at the parentId parameter. It is null, so it would only return roots
         };
         var customMeasurePointsResult = await client.SearchNodesAsync( form );
         var measurePoint = customMeasurePointsResult.Items.FirstOrDefault() as CustomMeasurePointDto;
         Console.WriteLine( "MeasurePoint: " + JsonConvert.SerializeObject( measurePoint, Formatting.Indented ) );
         Console.WriteLine( "---" );


         Console.WriteLine( "Press any key to continue..." );
         Console.ReadKey();

         // retriving measurements
         var to = DateTime.UtcNow;
         var from = to - TimeSpan.FromDays( 1 );
         var request = new MeasurementsRequest
         {
            To = to,
            From = from,
            Queries = new List<QueryDto>
            {
               new QueryDto( )
               {
                  Interval = SamplingInterval.Raw, // Currently only supports a subset of these: "Raw", "FiveMinutely", "Hourly", "Daily", "Monthly"... more to come
                  NodeId = measurePoint.Id
               }
            }
         };
         var measurements = await client.GetMeasurementsAsync( request );
         var result = measurements.Results[ measurePoint.Id.ToString() ];

         Console.WriteLine( "Data for measure point (simple): " );
         foreach( var record in result.Records )
         {
            Console.WriteLine( "Timestamp: " + record.Timestamp + ", Value: " + record.Value );
         }

         //Console.WriteLine( "Data for measure point (generic): " );
         //var mp = result.Query.Node as MeasurePointBaseDto;  // this will always be a measure point (also the same value as 'measurePoint')
         //foreach( var record in result.Records )
         //{
         //   Console.Write("Timestamp: " + record.Timestamp );
         //   foreach( var field in mp.MeasureType.Fields )
         //   {
         //      Console.Write( ", " + field.Name + ": " + record[ field.Id ] );
         //   }
         //   Console.WriteLine();
         //}

         Console.ReadKey();
      }
   }
}
