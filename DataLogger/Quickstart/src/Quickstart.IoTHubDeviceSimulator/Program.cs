﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Client;

namespace Insero.Platform2.IoTHubCustomProtocolSimulator
{
   class Program
   {
      private static DeviceClient DeviceClient;
      private readonly static string DeviceConnectionString = "HostName=AarhusIoTHub.azure-devices.net;DeviceId=maDevice;SharedAccessKey=YTWyx3CuJxiXDbgBtWse8+KTB1NVWqN24wF2Y9uZ3SM=";

      // Async method to send simulated telemetry
      private static async Task SendDeviceToCloudMessagesAsync()
      {
         // Initial telemetry values
         double currentValue = 20;
         Random rand = new Random();
         int n = 0;
         int expectedInterval = 10 * 1000;

         while( true )
         {
            currentValue = currentValue + ( rand.NextDouble() - 0.5 );

            // Create JSON message
            var telemetryDataPoint = new SingleDeviceCustomProtocolPayload
            {
               UserDefinedTypeName = "Simulator",
               MeasurePoints = new List<MeasurePointData>
               {
                  new MeasurePointData
                  {
                     Id = "Temperature",
                     Measurements = new List<Dictionary<string, object>>
                     {
                        new Dictionary<string, object>
                        {
                           { "timestamp", DateTime.UtcNow },
                           { "value", currentValue }
                        }
                     }
                  }
               }
            };

            // only send the metadata every 1000 message
            if( n++ % 1000 == 0 )
            {
               telemetryDataPoint.MeasurePoints[ 0 ].Metadata = new MeasurePointMetadata
               {
                  ExpectedInterval = expectedInterval,
                  Mode = MeasuringMode.Sample,
                  MeasureType = new MeasureTypeMetadata
                  {
                     Fields = new Dictionary<string, MeasureTypeFieldMetadata>
                     {
                        { "value", new MeasureTypeFieldMetadata { DataType = DataType.Double, Unit = "°C" } }
                     }
                  }
               };
            } 
            

            var messageString = PayloadSerializer.SerializeWithType( telemetryDataPoint );
            var message = new Message( Encoding.UTF8.GetBytes( messageString ) );

            // Send the telemetry message
            await DeviceClient.SendEventAsync( message );
                Console.WriteLine("!");

            await Task.Delay( expectedInterval );
         }
      }
      private static void Main( string[] args )
      {
         try
         {
            // Connect to the IoT hub using the MQTT protocol
            using( DeviceClient = DeviceClient.CreateFromConnectionString( DeviceConnectionString, TransportType.Mqtt ) )
            {
                   
               SendDeviceToCloudMessagesAsync().Wait();
            }
         }
         catch( Exception )
         {

         }
      }
   }
}
