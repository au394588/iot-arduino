﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insero.Platform2.IoTHubCustomProtocolSimulator
{
   [CustomJsonName( "protocol.custom.v1.single" )]
   public class SingleDeviceCustomProtocolPayload : DeviceData
   {
   }

   [CustomJsonName( "protocol.custom.v1.multi" )]
   public class MultiDeviceCustomProtocolPayload
   {
      public List<DeviceData> Devices { get; set; }
   }

   public class DeviceData
   {
      public string DeviceId { get; set; }

      public string UserDefinedTypeName { get; set; }

      public List<MeasurePointData> MeasurePoints { get; set; }
   }

   public class MeasurePointData
   {
      public string Id { get; set; }

      public MeasurePointMetadata Metadata { get; set; }

      public List<Dictionary<string, object>> Measurements { get; set; }
   }

   public class MeasurePointMetadata
   {
      public MeasuringMode Mode { get; set; }

      public int? ExpectedInterval { get; set; }

      public MeasureTypeMetadata MeasureType { get; set; }
   }
   
   public class MeasureTypeMetadata
   {
      public string Name { get; set; }

      public Dictionary<string, MeasureTypeFieldMetadata> Fields { get; set; }
   }

   public class MeasureTypeFieldMetadata
   {
      public string Unit { get; set; }

      public string Name { get; set; }

      public DataType DataType { get; set; }
   }

   public enum DataType
   {
      Long = 0,
      Double = 1,
      Decimal = 2,
      Bytes = 3,
      String = 4,
      Boolean = 5,
   }

   public enum MeasuringMode
   {
      Sample = 1,
      Average = 2,
      Accumulation = 3,
      Total = 4
   }

}
