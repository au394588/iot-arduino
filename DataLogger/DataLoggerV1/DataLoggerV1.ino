/*
 Name:		DataLoggerV1.ino
 Created:	9/24/2018 9:58:51 AM
 Author:	MortenMoos
*/

#pragma region Pre Setup

/*
This example does a test of the TCP client capability:
  * Initialization
  * Optional: SSID scan
  * AP connection
  * DHCP printout
  * DNS lookup
  * Optional: Ping
  * Connect to website and print out webpage contents
  * Disconnect
SmartConfig is still beta and kind of works but is not fully vetted!
It might not work on all networks!
*/
#include <Adafruit_CC3000.h>
#include <ccspi.h>
#include <SPI.h>
#include <string.h>
#include "utility/debug.h"
#include <Adafruit_AM2315.h>
#include <Adafruit_RGBLCDShield.h>

// These are the interrupt and control pins
#define CC3000_RX_BUFFER_SIZE (1300)
#define CC3000_TX_BUFFER_SIZE (1300)
#define ADAFRUIT_CC3000_IRQ   3  // MUST be an interrupt pin!
// These can be any two pins
#define ADAFRUIT_CC3000_VBAT  5
#define ADAFRUIT_CC3000_CS    10
// Use hardware SPI for the remaining pins
// On an UNO, SCK = 13, MISO = 12, and MOSI = 11
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT,
	SPI_CLOCK_DIV2); // you can change this clock speed

#define WLAN_SSID       "AU-Gadget"           // cannot be longer than 32 characters!
#define WLAN_PASS       "augadget"

// Security can be WLAN_SEC_UNSEC, WLAN_SEC_WEP, WLAN_SEC_WPA or WLAN_SEC_WPA2
#define WLAN_SECURITY   WLAN_SEC_WPA2

#define IDLE_TIMEOUT_MS  1000      // Amount of time to wait (in milliseconds) with no data 
								   // received before closing the connection.  If you know the server
								   // you're accessing is quick to respond, you can reduce this value.

#define CC3000_RX_BUFFER_SIZE (1300)
#define CC3000_TX_BUFFER_SIZE (1300)


// What page to grab!
//#define WEBSITE      "wifitest.adafruit.com"
//#define WEBPAGE      "/testwifi/index.html"

#define WEBSITE      "dataloggerservicesazurepoc.azurewebsites.net"
#define WEBPAGE      "/api/DataLogEntry/"

#pragma endregion



String targetURI = "";

// SENSOR DATA
Adafruit_AM2315 am2315;

// Variables for CO2  
const int analogPin_CO2 = A0;
// Setting Sensor Calibration Constants
float v400ppm = 3.63281;   //MUST BE SET ACCORDING TO CALIBRATION
float v5050ppm = 2.807617; //MUST BE SET ACCORDING TO CALIBRATION
float Delta_vs = v400ppm - v5050ppm;
float A = Delta_vs / (log10(420) - log10(5050));
float B = log10(420);

// Input pin for VOC  
const int analogPin_VOC = A2;

// Input pin for PIR
const int inputPin_PIR = 2;

// Input pin for FAN
const int outputPin_FAN = 6;

// Input pin for Noise
const int analogPin_NOISE = A3;

//this is the counter that will be used for logging data = id

int
PIR, val_PIR, CO2;
unsigned long
UNIX, LasttimePIR = 0;
float
HUM, TEMP, VOC, NOISE, val_NOISE;

float signalMin_NOISE = 3.3;
float signalMax_NOISE = 0;

uint32_t ip, sendCount;
Adafruit_CC3000_Client www;

////////////////////////////////////// LCD SCREEN /////////////////////////////////////////////////////////////////////
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();
#define ON 0x7
#define OFF 0x0

int pause_fan = 0;
unsigned long fan_p_start = 0;
unsigned long test_p = 0;

void setup(void)
{
	setupSerial();

	lcd.begin(16, 2);
	lcd.setBacklight(ON);
	lcd.print("Starting...");


	//UNIX_time();

	// Initializes sensors
	sensors_init();

	connectToTcp();

	//connectToSite();

	analogWrite(outputPin_FAN, 150);
	//

}

bool displayConnectionDetails(void)
{
	uint32_t ipAddress, netmask, gateway, dhcpserv, dnsserv;

	if (!cc3000.getIPAddress(&ipAddress, &netmask, &gateway, &dhcpserv, &dnsserv))
	{
		Serial.println(F("Unable to retrieve the f Address!\r\n"));
		return false;
	}
	else
	{
		Serial.print(F("\nIP Addr: ")); cc3000.printIPdotsRev(ipAddress);
		Serial.print(F("\nNetmask: ")); cc3000.printIPdotsRev(netmask);
		Serial.print(F("\nGateway: ")); cc3000.printIPdotsRev(gateway);
		Serial.print(F("\nDHCPsrv: ")); cc3000.printIPdotsRev(dhcpserv);
		Serial.print(F("\nDNSserv: ")); cc3000.printIPdotsRev(dnsserv);
		Serial.println();
		return true;
	}
}

void Sensor_read_PIR()
{
	val_PIR = digitalRead(inputPin_PIR);
	if (PIR == 1)
	{
		PIR = 1;
	}
	else
	{
		PIR = val_PIR;
	}
	//return PIR;
}

void Sensor_read_NOISE()
{
	val_NOISE = (analogRead(analogPin_NOISE)) / 310.30303;

	if (val_NOISE < 3.3000)  // toss out spurious readings
	{
		if (val_NOISE > signalMax_NOISE)
		{
			signalMax_NOISE = val_NOISE;  // save just the max levels
		}
		else if (val_NOISE < signalMin_NOISE)
		{
			signalMin_NOISE = val_NOISE;  // save just the min levels
		}
	}

	NOISE = signalMax_NOISE - signalMin_NOISE;

	if (NOISE < 0)
	{
		NOISE = 0;
	}

	//return NOISE;
}

void Log_data() {
	Serial.println("Log_data");
	//execute our data handlers to get data to send back to base station

	//UNIX = lastPolledTime + (millis() - sketchTime) / 1000 + 3600;

	am2315.readTemperatureAndHumidity(TEMP, HUM);

	CO2 = round((pow(10, ((((analogRead(analogPin_CO2) / 204.8) - v400ppm) / A) + B))) / 10) * 10;

	VOC = (analogRead(analogPin_VOC) / 204.8);
	Sensor_read_PIR();
	//build the URI and GET args
	//we have to do this within the loop to get fresh values for data
	//targetURI = "";
	//targetURI = targetURI + WEBPAGE + "?URL=";
	////targetURI = targetURI + "id=" + pollCounter;
	//targetURI = targetURI + "&I=" + "1";
	////targetURI = targetURI + "&GeoLocation=" + "45.00001,20.0033";
	////targetURI = targetURI + "&UnixTime=" + UNIX;
	//targetURI = targetURI + "&H=" + HUM;
	//targetURI = targetURI + "&C=" + TEMP;
	//targetURI = targetURI + "&Co2=" + CO2;
	//targetURI = targetURI + "&VOC=" + VOC;
	//targetURI = targetURI + "&PIR=" + PIR;
	//targetURI = targetURI + "&N=" + NOISE;
	

	targetURI = "";
	
	//targetURI = targetURI + "id=" + pollCounter;
	targetURI = "1";//DeviceID
	//targetURI = targetURI + "&GeoLocation=" + "45.00001,20.0033";
	//targetURI = targetURI + "&UnixTime=" + UNIX;
	targetURI = targetURI + "," + HUM;
	targetURI = targetURI + "," + TEMP;
	targetURI = targetURI + "," + CO2;
	targetURI = targetURI + "," + VOC;
	targetURI = targetURI + "," + PIR;
	targetURI = targetURI + "," + NOISE;

    //http://dataloggerservicesazurepoc.azurewebsites.net/api/DataLogEntry/1%2C1,2.2,3.3,4.4,5.5,6.6,7.7
	//targetURI = "sdf,11,1,1,1,1,1";
	PIR = 0;
	NOISE = 0;
	signalMin_NOISE = 3.3;
	signalMax_NOISE = 0;



	//pollCounter = pollCounter + 1;  //increment the counter
	//return targetURI;
	Serial.print(F("TargetURI: ")); Serial.println(targetURI);
	Serial.print(F("Free RAM: ")); Serial.println(getFreeRam(), DEC);
}

void sensors_init() {
	Serial.println(F("Initializing sensors"));


	if (!am2315.begin()) {
		Serial.println("Sensor not found, check wiring & pullups!");
		lcd.clear();
		lcd.print("Sensor not found");
		while (1);
	}

}

#pragma region unixtime



//void UNIX_time(void)
//{
	//Adafruit_CC3000_Client client;

	//uint8_t       buf[48];
	//unsigned long ip, startTime, t = 0L;
	//long gettime_1;

	//Serial.print(F("Locating time server..."));

	//// Hostname to IP lookup; use NTP pool (rotates through servers)
	//if (cc3000.getHostByName("dk.pool.ntp.org", &ip)) {
	//	static const char PROGMEM
	//		timeReqA[] = { 227,  0,  6, 236 },
	//		timeReqB[] = { 49, 78, 49,  52 };

	//	Serial.println(F("\r\nAttempting connection..."));
	//	do {
	//		client = cc3000.connectUDP(ip, 123);
	//	} while ((!client.connected())); //&& ((millis() - startTime) < connectTimeout)
	//	if (client.connected()) {
	//		Serial.print(F("connected!\r\nIssuing request..."));
	//		// Assemble and issue request packet
	//		memset(buf, 0, sizeof(buf));
	//		memcpy_P(buf, timeReqA, sizeof(timeReqA));
	//		memcpy_P(&buf[12], timeReqB, sizeof(timeReqB));
	//		client.write(buf, sizeof(buf));

	//		Serial.print(F("\r\nAwaiting response..."));
	//		memset(buf, 0, sizeof(buf));
	//		startTime = millis();
	//		while ((!client.available()) && (millis() - startTime) < responseTimeout);
	//		if (client.available()) {
	//			client.read(buf, sizeof(buf));

	//			t = (((unsigned long)buf[40] << 24) |
	//				((unsigned long)buf[41] << 16) |
	//				((unsigned long)buf[42] << 8) |
	//				(unsigned long)buf[43]) - 2208988800UL;
	//			Serial.print(F("OK\r\n"));

	//		}
	//		client.close();
	//	}
	//}
	//if (!t) Serial.println(F("error"));

	//lastPolledTime = t;         // Save time
	//sketchTime = millis();  // Save sketch time of last valid time query

//}

#pragma endregion

void connectToSite(void)
{
	Serial.print(F(WEBSITE)); Serial.print(F(" -> "));
	while (ip == 0) {
		if (!cc3000.getHostByName(WEBSITE, &ip)) {
			Serial.println(F("Couldn't resolve!"));
		}
		delay(500);
		Serial.print(F("!"));
	}
	Serial.print(F(" -> "));
	cc3000.printIPdotsRev(ip);
	Serial.print(F(" -> "));
	www = cc3000.connectTCP(ip, 80);
}

void connectToTcp(void)
{
	int timeout = 0;

	Serial.print(F("\nAttempting to connect to ")); Serial.println(WLAN_SSID);
	lcd.clear();
	lcd.print("Connect to SSID                         ");
	
	lcd.print(WLAN_SSID);

	if (!cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY)) {
		Serial.println(F("Failed!"));		
			
			lcd.clear();			
			lcd.print("Missing net..");
			
			if (timeout == 5000) /*aprox 8 min*/
			{
				cc3000.disconnect();
				cc3000.reboot();
				delay(1000);
				connectToTcp();
			}
			timeout++;
			
			lcd.println();
			lcd.print(timeout);
	}

	Serial.println(F("Connected!"));

	/* Wait for DHCP to complete */
	Serial.println(F("Request DHCP"));
	timeout = 0;

	while (!cc3000.checkDHCP())
	{
		if (timeout == 5000) /*aprox 8 min*/
		{
			cc3000.disconnect();
			cc3000.reboot();
			delay(1000);
			connectToTcp();
		}
		
		timeout++;
		lcd.clear();
		lcd.print("DHCP...");
		lcd.println();
		lcd.print(timeout);
		
		delay(100); 
	}

	/* Display the IP address DNS, Gateway, etc. */
	while (!displayConnectionDetails()) {
		delay(1000);
	}

	ip = 0;
	sendCount = 0;
}

void setupSerial(void)
{
	//Serial.begin(115200);
	Serial.begin(9600);
	Serial.println(F("Hello, CC3000!\n"));

	Serial.print(F("Free RAM: ")); Serial.println(getFreeRam(), DEC);

	/* Initialise the module */
	Serial.println(F("\nInitializing..."));
	if (!cc3000.begin())
	{
		Serial.println(F("Couldn't begin()! Check your wiring?"));
		while (1);
	}

	// set pinmode
	/*pinMode(analogPin_CO2, INPUT);
	pinMode(analogPin_VOC, INPUT);
	pinMode(analogPin_NOISE, INPUT);
	pinMode(inputPin_PIR, INPUT);
	pinMode(outputPin_FAN, OUTPUT);*/
}

void loop(void)
{
	
	Serial.print(F("Free RAM: ")); Serial.println(getFreeRam(), DEC);

	PIR = 0;
	NOISE = 0;
	signalMin_NOISE = 3.3;
	signalMax_NOISE = 0;

	Log_data();

	www.flush();
	www.close();
	www.stop();
	connectToSite();
	
	if (www.connected()) {
		
		uint8_t replies = cc3000.ping(ip, 1);
		//if (replies > 0)
		//{
			sendCount++;
			lcd.clear();
			lcd.print("|Sending HTTPGET|");			
			lcd.print(String(sendCount));
			Serial.println(F("Starting to send get command"));
			www.fastrprint(F("GET "));
			Serial.print("GET ");
			www.fastrprint(F(WEBPAGE)); www.print(targetURI); /*www.fastrprint(F("?VOC=2.00"));*/
			Serial.print(WEBPAGE); Serial.print(targetURI);
			www.fastrprint(F(" HTTP/1.1\r\n"));
			Serial.print(" HTTP/1.1\r\n");
			www.fastrprint(F("Host: ")); www.fastrprint(F(WEBSITE));   www.fastrprint(F("\r\n"));
			Serial.print("Host: "); Serial.print(WEBSITE); Serial.print("\r\n");
			www.fastrprint(F("\r\n"));
			Serial.print("\r\n");
			www.println();
			Serial.println();


		/*	
			www.fastrprint(F("GET ""http://dataloggerservicesazurepoc.azurewebsites.net/api/DataLogEntry/DeviceName1,1,1,1,1,1,1"" -H accept: text/plain"));
			www.println();
*/
			

			Serial.println(F("ended to send get command"));
		//}
		//else
			//delay(2000);
		lcd.clear();
		//lcd.print(".");

	}
	else {
		Serial.println(F("Connection failed"));
		lcd.clear();
		lcd.print("Connection failed");
		connectToTcp();

		//connectToSite();
		//return;
	}



	Serial.println(F("-------------------------------------"));

	/* Read data until either the connection is closed, or the idle timeout is reached. */
	unsigned long lastRead = millis();
	while (www.connected() && (millis() - lastRead < IDLE_TIMEOUT_MS)) {
		while (www.available()) {
			char c = www.read();
			Serial.print(c);
			lastRead = millis();
		}
	}

	Serial.println(F("-------------------------------------"));



	//Serial.println(F("Pinging ")); 
	//lcd.println("Pinging");
	//cc3000.printIPdotsRev(ip); 
	//Serial.print(F("..."));
	//uint8_t replies = cc3000.ping(ip, 1);
	//Serial.print(replies); 
	//Serial.println(F(" replies"));
	//
	//if (replies > 0)
	//{
	//	lcd.clear();
	//	lcd.println("Ping successful!");
	//	lcd.print(sendCount);
	//	Serial.println(F("Ping successful!"));
	//}
	//else
	//{
	//	lcd.clear();
	//	lcd.println("Refreshing con.");
	//	lcd.print(sendCount);
	//	Serial.println(F("Ping unsuccessful!..flush...close...stop...reconnect to site..."));
	//	www.flush();
	//	www.close();
	//	www.stop();
	//	connectToSite();
	//}

	//if(cc3000.checkConnected())
	//	Serial.println(F("cc3000.checkConnected()"));
	//else
	//	Serial.println(F("!cc3000.checkConnected()"));
	//
	//if (cc3000.checkDHCP())
	//	Serial.println(F("cc3000.checkDHCP"));
	//else
	//	Serial.println(F("!cc3000.checkDHCP"));	

	//if(www.available())
	//	Serial.println(F("www.available()"));
	//else
	//	Serial.println(F("!www.available()"));
	//
	//if (www.connected())
	//	Serial.println(F("www.connected()"));
	//else
	//	Serial.println(F("!www.connected()"));



	//www.flush();
	//www.close();
	Serial.println(F("-------------------------------------"));
	delay(25000);


	/*if (buttons) {
		lcd.clear();
		lcd.setCursor(0, 0);
		if (buttons & BUTTON_SELECT)
		{
			fan_p_start = millis();
			pause_fan = 1;
			lcd.setBacklight(ON);
			lcd.print("Fan paused ");
			lcd.setCursor(0, 1);
			lcd.print("for 30 min.");

		}
	}
	if (pause_fan == 1)
	{

		test_p = millis() - fan_p_start;
		if (test_p <= 1800 * 1000)
		{
			analogWrite(outputPin_FAN, 0);
		}
		else if (test_p >= 1800 * 1000)
		{
			pause_fan = 0;
			fan_p_start = 0;
			analogWrite(outputPin_FAN, 255);
			lcd.clear();
			lcd.setCursor(0, 0);
			lcd.setBacklight(OFF);
		}
	}*/
}







