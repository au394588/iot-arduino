﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataloggerServices.Models
{
    public class DataLogEntry
    {
        public int Id { get; set; }
        public string UnitId { get; set; }
        public string DeviceTimeStamp { get; set; }
        public decimal Temperature { get; set; }
        public decimal Humidity { get; set; }
        public decimal Co2 { get; set; }
        public decimal VOC { get; set; }
        public decimal PIR { get; set; }
        public decimal Noise { get; set; }
    }
}
