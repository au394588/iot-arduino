﻿namespace DataloggerServices.Models
{
    public class SimpleDataLogEntry : BaseEntity
    {
        public string DeviceId { get; set; }
        public string DeviceTimeStamp { get; set; }
        public decimal Temperature { get; set; }
        public decimal Humidity { get; set; }
        public decimal Co2 { get; set; }
        public decimal VOC { get; set; }
        public decimal Noise { get; set; }
        public decimal PIR { get; set; }
    }
}
