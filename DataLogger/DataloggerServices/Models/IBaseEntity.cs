﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataloggerServices.Models
{
    interface IBaseEntity : IDisposable
    {
        Int32 Id { set; get; }
    }
}
