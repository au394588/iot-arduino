﻿using DataloggerServices.Models;
using Microsoft.Azure.Devices.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataloggerServices.InseroIOTHubConnectionUtils
{
    public class IOTHubApplicationContext
    {
        private  DeviceClient DeviceClient;
        private string _iotConnectionString;
        
        public IOTHubApplicationContext(string iotconnectionstring)
        {
            _iotConnectionString = iotconnectionstring; // "HostName=AarhusIoTHub.azure-devices.net;DeviceId=mmodevice01;SharedAccessKey=GjqHwsxbPuZjbmIZrOjeuzrg9G9Cn+2ZsOZNpczPgFI=" /*IOTConnectionString*/;
            //_iotConnectionString_blar = "HostName=AarhusIoTHub.azure-devices.net;DeviceId=blar;SharedAccessKey=GjqHwsxbPuZjbmIZrOjeuzrg9G9Cn+2ZsOZNpczPgFI=" /*IOTConnectionString*/;

        }

        public void SaveChanges(SimpleDataLogEntry s)
        {            
            try
            {
                // Connect to the IoT hub using the MQTT protocol
                using (DeviceClient = DeviceClient.CreateFromConnectionString(_iotConnectionString, TransportType.Mqtt))
                {
                    SendDeviceToCloudMessagesAsync(s).Wait();
                }
            }
            catch (Exception e)
            {

            }
        }

        // Async method to send simulated telemetry
        private async Task SendDeviceToCloudMessagesAsync(SimpleDataLogEntry s)
        {
            //Enrich parameterlist /*rewrite to generics */
            var listOfSensorValues = new List<LogParameter>();
            listOfSensorValues.Add(new LogParameter { DeviceId = s.DeviceId, ExpectedInterval = 6000, Name = "PIR", Unit = "Lux", Value = (double)s.PIR });
            listOfSensorValues.Add(new LogParameter { DeviceId = s.DeviceId, ExpectedInterval = 6000, Name = "Temperature", Unit = "°C", Value = (double)s.Temperature });
            listOfSensorValues.Add(new LogParameter { DeviceId = s.DeviceId, ExpectedInterval = 6000, Name = "VOC", Unit = "unknown", Value = (double)s.VOC });
            listOfSensorValues.Add(new LogParameter { DeviceId = s.DeviceId, ExpectedInterval = 6000, Name = "Co2", Unit = "PPM", Value = (double)s.Co2 });
            listOfSensorValues.Add(new LogParameter { DeviceId = s.DeviceId, ExpectedInterval = 6000, Name = "Noise", Unit = "dBm", Value = (double)s.Noise });
            listOfSensorValues.Add(new LogParameter { DeviceId = s.DeviceId, ExpectedInterval = 6000, Name = "Humidity", Unit = "%", Value = (double)s.Humidity });
                                                  
            // Create JSON message
            foreach(LogParameter l in listOfSensorValues)
            {
                if (l.Value != -9999999.00)
                {
                    await DeviceClient.SendEventAsync(createInseroIotMessage(l.DeviceId, l.Value, l.ExpectedInterval, l.Name, l.Unit));
                }
            }
        }

        private static Message createInseroIotMessage(string deviceId,double currentValue, int expectedInterval, string sensorMeasurementName, string sensorMeasurementUnit)
        {
            var telemetryDataPoint = new SingleDeviceCustomProtocolPayload
            {
                DeviceId = deviceId,
                UserDefinedTypeName = "Arduino",
                MeasurePoints = new List<MeasurePointData>
                {
                  new MeasurePointData
                  {
                     Id = sensorMeasurementName,
                     Measurements = new List<Dictionary<string, object>>
                     {
                        new Dictionary<string, object>
                        {
                           { "timestamp", DateTime.UtcNow },
                           { "value", currentValue }
                        }
                     }
                  }
                }
            };

            // only send the metadata every 1000 message
            //if (n++ % 1000 == 0)
            //{
            telemetryDataPoint.MeasurePoints[0].Metadata = new MeasurePointMetadata
            {
                ExpectedInterval = expectedInterval,
                Mode = MeasuringMode.Sample,
                MeasureType = new MeasureTypeMetadata
                {
                    Fields = new Dictionary<string, MeasureTypeFieldMetadata>
                     {
                        { "value", new MeasureTypeFieldMetadata { DataType = DataType.Double, Unit = sensorMeasurementUnit } }
                     }
                }
            };
            //}


            var messageString = PayloadSerializer.SerializeWithType(telemetryDataPoint);
            var message = new Message(Encoding.UTF8.GetBytes(messageString));
            return message;
        }
    }

    class LogParameter
    {
        public string DeviceId { get; set; }
        public string Name { get; set; }
        public double Value { get; set; }
        public string Unit { get; set; }
        public int ExpectedInterval { get; set; }
    }
}

   
   

