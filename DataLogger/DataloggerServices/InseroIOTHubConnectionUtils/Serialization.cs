﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace DataloggerServices
{

   [AttributeUsage( AttributeTargets.Class, AllowMultiple = false, Inherited = false )]
   public sealed class CustomJsonNameAttribute : Attribute
   {
      public CustomJsonNameAttribute( string name )
      {
         Name = name;
      }

      public string Name { get; }
   }

   public static class PayloadSerializer
   {
      public static readonly JsonSerializerSettings DefaultSettings = JsonSettings.Create();

      public static readonly JsonSerializer Default = JsonSerializer.Create( DefaultSettings );

      public static string SerializeWithType<T>( T obj )
      {
         return JsonConvert.SerializeObject( obj, typeof( object ), DefaultSettings );
      }

      public static T Deserialize<T>( string json )
      {
         return JsonConvert.DeserializeObject<T>( json, DefaultSettings );
      }
   }


   public static class JsonSettings
   {
      public static JsonSerializerSettings Create()
      {
         var settings = new JsonSerializerSettings();
         settings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
         settings.Formatting = Formatting.None;
         settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
         settings.SerializationBinder = ApplicationSerializationBinder.Default;
         settings.TypeNameHandling = TypeNameHandling.Auto;
         settings.Converters.Add( new StringEnumConverter() { CamelCaseText = true } );

         return settings;
      }
   }

   public class ApplicationSerializationBinder : DefaultSerializationBinder
   {
      public static readonly ApplicationSerializationBinder Default = new ApplicationSerializationBinder();

      private readonly ConcurrentDictionary<string, Type> _customNameToClrTypeLookups;
      private readonly ConcurrentDictionary<Type, string> _clrTypeToCustomNameLookups;

      private ApplicationSerializationBinder()
      {
         _customNameToClrTypeLookups = new ConcurrentDictionary<string, Type>();
         _clrTypeToCustomNameLookups = new ConcurrentDictionary<Type, string>();
      }

      public override void BindToName( Type serializedType, out string assemblyName, out string typeName )
      {
         var customName = GetCustomName( serializedType );
         if( customName != null )
         {
            assemblyName = null;
            typeName = customName;
         }
         else
         {
            base.BindToName( serializedType, out assemblyName, out typeName );
         }
      }

      public override Type BindToType( string assemblyName, string typeName )
      {
         var clrType = GetClrType( typeName );
         if( clrType != null )
         {
            return clrType;
         }
         else
         {
            return base.BindToType( assemblyName, typeName );
         }
      }

      private Type GetClrType( string nmsType )
      {
         if( !_customNameToClrTypeLookups.TryGetValue( nmsType, out Type clrType ) )
         {
            foreach( var assembly in AppDomain.CurrentDomain.GetAssemblies() )
            {
               try
               {
                  foreach( var type in assembly.GetTypes() )
                  {
                     var attr = type.GetCustomAttribute<CustomJsonNameAttribute>( inherit: false );
                     if( attr != null && attr.Name == nmsType )
                     {
                        clrType = type;
                        break;
                     }
                  }
               }
               catch( Exception )
               {
                  // ignored
               }
            }

            _customNameToClrTypeLookups[ nmsType ] = clrType;
         }

         return clrType;
      }

      private string GetCustomName( Type clrType )
      {
         if( !_clrTypeToCustomNameLookups.TryGetValue( clrType, out string customName ) )
         {
            var attribute = clrType.GetCustomAttribute<CustomJsonNameAttribute>( inherit: false );
            customName = attribute?.Name;
            _clrTypeToCustomNameLookups[ clrType ] = customName;
         }

         return customName;
      }
   }
}
