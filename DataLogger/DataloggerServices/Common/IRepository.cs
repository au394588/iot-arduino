﻿using DataloggerServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataloggerServices.Common
{
    public interface IRepository<TEntity> : IDisposable where TEntity : BaseEntity
    {
        IQueryable<TEntity> GetAll();

        TEntity GetById(long id);

        TEntity Create(TEntity entity);

        IQueryable<TEntity> DoSQL(string sql);

        int BulkOperation(string sql);
    }
}
