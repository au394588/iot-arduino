﻿using DataloggerServices.InseroIOTHubConnectionUtils;
using DataloggerServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using Microsoft.EntityFrameworkCore;

namespace DataloggerServices.Common
{
    public class IOTRepository<TEntity> : IRepository<TEntity>
    where TEntity : BaseEntity
    {
        private IOTHubApplicationContext _context;
       // private DbSet<TEntity> _entity;

        public IOTRepository(IOTHubApplicationContext context)
        {
            _context = context;
            //_entity = _context.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            //return _entity.AsNoTracking();
            throw new NotImplementedException();
        }

        public TEntity GetById(long id)
        {
            //return _entity.FirstOrDefault(s => s.Id == id);
            throw new NotImplementedException();
        }
        //https://localhost:44340/api/DataLogEntry/1,1,1,1,1,1,1
        public TEntity Create(TEntity entity)
        {
            var entObj = (Object)entity;
            var s = (SimpleDataLogEntry)entObj;
            
            _context.SaveChanges(s);
            //return createdEntity.Entity;
            //SendIotTest();
            return entity;
        }



        public IQueryable<TEntity> DoSQL(string sql)
        {
            //return _entity.FromSql(sql);
            return null;
        }

        public int BulkOperation(string sql)
        {
            // return _context.Database.ExecuteSqlCommand(sql);
            return -1;
        }

        public void Dispose()
        {

        }
    }
}
