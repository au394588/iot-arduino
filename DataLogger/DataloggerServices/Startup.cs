﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;
using AutoMapper;
using System;
using DataloggerServices.Models;
using DataloggerServices.Common;
using Microsoft.Azure.Devices.Client;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using DataloggerServices.InseroIOTHubConnectionUtils;

namespace DataloggerServices
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public static IContainer Container { get; private set; }
        public static IServiceProvider ServiceProvider { get; private set; }
        public static ApplicationContext DbContext { get { return ServiceProvider.GetService<ApplicationContext>(); } }
        public static string IOTConnectionString { get;private set; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //var connectionString = Configuration.GetConnectionString("IOTSQL");
            //services
            //    .AddDbContext<ApplicationContext>(options => options.UseSqlServer(connectionString))
            //    .AddSingleton<IDbContextOptionsBuilderInfrastructure>(new DbContextOptionsBuilder().UseSqlServer(connectionString))
            //    .AddScoped(typeof(IRepository<>), typeof(Repository<>));


            var connectionString = Configuration.GetConnectionString("IOTHub");
            services
                .AddSingleton<IOTHubApplicationContext>(new IOTHubApplicationContext(connectionString))
                .AddScoped(typeof(IRepository<>), typeof(IOTRepository<>));

            //poc 
            //make new repo : irepo
            //add services add connection to iot hub.
            //make connection to iot hub

            services.AddScoped(provider => new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutomapperProfile(provider.GetService<IRepository<SimpleDataLogEntry>>()));

            }).CreateMapper());

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "DataLoggerServices",
                    Version = "v1",
                    // You can also set Description, Contact, License, TOS...
                });

                // Configure Swagger to use the xml documentation file
                // var xmlFile = Path.ChangeExtension(typeof(Startup).Assembly.Location, ".xml");
                //var xmlFile = Path.ChangeExtension(@"C:\Users\MortenMoos\OneDrive - Morten Moos APS\Dokumenter\Source\Repos\talentsoft\KlassifikationServices", ".xml");

                //c.IncludeXmlComments(xmlFile);
            });

            var builder = new ContainerBuilder();
            
            builder.Populate(services);

            // populate autofac IOC here

            Container = builder.Build();
            ServiceProvider = new AutofacServiceProvider(Container);

            

        }

       

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //app.UseHttpsRedirection(); //disabled due to arduino cc3000 board does not support ssl.
            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "DataLogServices");
            });
        }

        



    }
}
