﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

using DataloggerServices.Common;
using DataloggerServices.Models;
using Microsoft.AspNetCore.Mvc;

namespace DataloggerServices.Controllers
{
    [Route("api/DataLogEntry")]
    [ApiController]
    public class DataLogController : ControllerBase
    {
        private IRepository<SimpleDataLogEntry> _dataLogRepo;
        private readonly IMapper _mapper;

        public DataLogController(IRepository<SimpleDataLogEntry> repository, IMapper mapper)
        {
            _dataLogRepo = repository;
            _mapper = mapper;
        }

        [HttpGet(Name = "GetAll")]
        public ActionResult<List<DataLogEntry>> GetAll()
        {
            //List<SimpleDataLogEntry> returnObject = new List<SimpleDataLogEntry>();
            var returnObject = _dataLogRepo.GetAll().ToList();

            if (returnObject == null || returnObject.Count() == 0)
                return new List<DataLogEntry>();

            return _mapper.Map<List<DataLogEntry>>(returnObject); ;
        }

        /*
         	targetURI = "";
	targetURI = targetURI + WEBPAGE + "?URL=";
	//targetURI = targetURI + "id=" + pollCounter;
	targetURI = targetURI + "&I=" + "SQUID001";
	//targetURI = targetURI + "&GeoLocation=" + "45.00001,20.0033";
	//targetURI = targetURI + "&UnixTime=" + UNIX;
	targetURI = targetURI + "&H=" + HUM;
	targetURI = targetURI + "&C=" + TEMP;
	targetURI = targetURI + "&Co2=" + CO2;
	targetURI = targetURI + "&VOC=" + VOC;
	targetURI = targetURI + "&PIR=" + PIR;
	targetURI = targetURI + "&N=" + NOISE;
         * */
        //https://localhost:44340/api/DataLogEntry/unit1,1,1,1,1,1,1?F=1
        [HttpGet("{I},{H},{C},{Co2},{VOC},{PIR},{N}", Name = "SetSensorValues")]
        public ActionResult<List<DataLogEntry>> GetAll(string I, decimal H, decimal C, decimal Co2, decimal VOC, decimal PIR, decimal N)
        {
            List<SimpleDataLogEntry> returnObject = new List<SimpleDataLogEntry>();
            
           
            _dataLogRepo.Create(new SimpleDataLogEntry() { Id=0,DeviceId = I, Humidity = H, Co2 = Co2, Temperature = C, VOC = VOC, Noise = N,PIR =PIR });

            if (returnObject == null)
                return new List<DataLogEntry>();

            return  _mapper.Map<List<DataLogEntry>>(returnObject);
        }
        [HttpGet("{Id}/{value},{type}", Name = "SetSensorValue")]
        public void GetSingleValue(string Id, decimal value, string type)
        {

        }

            //todo i aften lav kolonner i sql på azure, få datalogger services op i azure
            // få arduino til at smide data op.
        }
}
