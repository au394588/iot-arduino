/**
   reuseConnection.ino

	Created on: 22.11.2015

*/


#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>

#define USE_SERIAL Serial

ESP8266WiFiMulti WiFiMulti;

HTTPClient http;

const int inputPin_PIR = 5;

int intervalIOTPirMessage = 30000;
unsigned long previousMillisPirCheck = 0;

int i = 1;
int val_PIR = 0;
int PIR = 0;

void setup()
{
	
	USE_SERIAL.begin(115200);
	// USE_SERIAL.setDebugOutput(true);

	USE_SERIAL.println();
	USE_SERIAL.println();
	USE_SERIAL.println();
	
	for (uint8_t t = 4; t > 0; t--)
	{
		USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
		USE_SERIAL.flush();
		delay(1000);
	}

	WiFi.mode(WIFI_STA);
	WiFiMulti.addAP("AU-Gadget", "augadget");

	// allow reuse (if server supports it)
	http.setReuse(true);

	pinMode(inputPin_PIR, INPUT);
	pinMode(LED_BUILTIN, OUTPUT);
}


void Sensor_read_PIR()
{
	val_PIR = digitalRead(inputPin_PIR);
	if (val_PIR == 1)
	{
		PIR = 1;
	}	
}

void loop()
{	
	unsigned long currentMillis = millis();
	
	Sensor_read_PIR();
	if (PIR == 1) {
		digitalWrite(LED_BUILTIN, HIGH);
	}
	else {
		digitalWrite(LED_BUILTIN, LOW);		
	}

	if ((unsigned long)(currentMillis - previousMillisPirCheck) >= intervalIOTPirMessage) {

		previousMillisPirCheck = currentMillis;
		USE_SERIAL.printf("PIR val %pir...count %i...\n", PIR, i);
		if ((WiFiMulti.run() == WL_CONNECTED)) {
			http.begin("http://dataloggerservicesazurepoc.azurewebsites.net/api/DataLogEntry/NodeMCU01,-9999999.00,-9999999.00,-9999999.00,-9999999.00," + String(PIR) + ",-9999999.00");

			int httpCode = http.GET();

			if (httpCode > 0) {
				USE_SERIAL.printf("[HTTP] GET... code: %d\n", httpCode);

				// file found at server
				if (httpCode == HTTP_CODE_OK) {
					i++;
					http.writeToStream(&USE_SERIAL);
				}
			}
			else {
				USE_SERIAL.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
			}

			http.end();
			PIR = 0;
			val_PIR = 0;
		}
	}

	
}


