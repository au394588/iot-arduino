﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Au.Datalogger.Backend.Model;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;

namespace Au.Datalogger.Backend
{
    /// <summary>
    /// Background service that keeps an eye on updates to any room status and broadcasts this to
    /// subscribers using SignalR.
    ///
    /// Currently only way to check the source for updates is polling.
    /// </summary>
    public class VacancyUpdaterService : BackgroundService
    {
        private readonly IHubContext<RoomStatusHub> _hubContext;
        private readonly IVacancyInfoRepository _repo;
        private List<RoomVacancy> _knownRooms;

        public VacancyUpdaterService(IHubContext<RoomStatusHub> hubContext,
            IVacancyInfoRepository repo)
        {
            _hubContext = hubContext;
            _repo = repo;
            _knownRooms = new List<RoomVacancy>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Trace.WriteLine($"VacancyUpdaterService is starting.");

            stoppingToken.Register(() =>
                Trace.WriteLine($"VacancyUpdate background task is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                Trace.WriteLine($"VacancyUpdate task doing background work.");

                RoomVacancy[] updates = await CheckVacancy();
                await _hubContext.Clients.All.SendAsync("vacancyUpdate", updates, cancellationToken: stoppingToken);

                await Task.Delay(5000, stoppingToken); // 5 second interval of polling
            }

            Trace.WriteLine($"VacancyUpdate background task is stopping.");
        }

        private async Task<RoomVacancy[]> CheckVacancy()
        {
            // Poll all room's statuses
            // Might not scale well, this list is kept in memory (but only one list per server).
            var dataState = await _repo.Get();

            // Check for added / updated / removed
            var changes = new List<RoomVacancy>();
            // Updated
            var updatedRooms = dataState.Where(s =>
                _knownRooms.Any(x => x.Id == s.Id)
                && _knownRooms.Single(x => x.Id == s.Id).RoomState != s.RoomState).ToArray();
            foreach (var updatedRoom in updatedRooms)
            {
                var room = _knownRooms.Single(x => x.Id == updatedRoom.Id);
                room.RoomState = updatedRoom.RoomState;
                room.Updated = updatedRoom.Updated;
            }
            changes.AddRange(updatedRooms);
            // New
            var newRooms = dataState.Where(s => _knownRooms.All(x => x.Id != s.Id)).ToArray();
            _knownRooms.AddRange(newRooms);
            changes.AddRange(newRooms);
            // Removed
            var removedRooms = _knownRooms.Where(r =>
                dataState.All(x => x.Id != r.Id)).ToArray();
            foreach (var removedRoom in removedRooms)
                _knownRooms.Remove(removedRoom);

            return changes.ToArray();
        }
    }
}
