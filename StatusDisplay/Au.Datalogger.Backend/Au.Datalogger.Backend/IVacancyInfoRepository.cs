﻿using System.Threading.Tasks;
using Au.Datalogger.Backend.Model;

namespace Au.Datalogger.Backend
{
    public interface IVacancyInfoRepository
    {
        Task<RoomVacancy[]> Get(string query = null);
    }
}