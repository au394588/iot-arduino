﻿using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Au.Datalogger.Backend.Model;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace Au.Datalogger.Backend
{
    public class VacancyInfoRepository : IVacancyInfoRepository
    {
        private readonly IConfiguration _config;

        public VacancyInfoRepository(IConfiguration config)
        {
            _config = config;
        }

        public async Task<RoomVacancy[]> Get(string query = null)
        {
            var connString = _config.GetConnectionString("Default");

            using (var conn = new SqlConnection(connString))
            {
                string sql = "select top 10 * from RoomVacancy ";
                if (!string.IsNullOrEmpty(query))
                    sql += "where Building=@Building OR Room=@Room";
                await conn.OpenAsync();
                return conn.Query<RoomVacancy>(sql,
                        new { Building = query, Room = query })
                    .ToArray();
            }
        }

    }
}