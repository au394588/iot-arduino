﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Au.Datalogger.Backend.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace Au.Datalogger.Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomVacancyController : ControllerBase
    {
        private readonly IVacancyInfoRepository _repo;

        public RoomVacancyController(IVacancyInfoRepository repo)
        {
            _repo = repo;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RoomVacancy>>> Get(string query)
        {
            return await _repo.Get(query);
        }
    }
}
