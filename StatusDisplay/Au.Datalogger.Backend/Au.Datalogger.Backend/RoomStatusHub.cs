﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Au.Datalogger.Backend
{
    public class RoomStatusHub : Hub
    {
        public async Task SendMessage(string building, string room, bool vacant)
        {
            await Clients.All.SendAsync("vacancyUpdate", building, room, vacant);
        }
    }
}
