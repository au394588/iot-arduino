import React, { Component } from 'react';
import './App.css';
import * as SignalR from '@aspnet/signalr'

class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      rooms: []
    }

    // This binding is necessary to make `this` work in the callback
    this.searchVacancies = this.searchVacancies.bind(this);
    this.changeSearchText = this.changeSearchText.bind(this);
  }

  hubConnection;

  componentDidMount() {
    this.hubConnection = new SignalR.HubConnectionBuilder()
      .withUrl(process.env.REACT_APP_BACKEND_API_URL + "roomstatushub")
      .configureLogging(SignalR.LogLevel.Information)
      .build();
    this.hubConnection.on("vacancyUpdate", (data) => this.vacancyUpdate(data));
    console.log("Backend: " + process.env.REACT_APP_BACKEND_API_URL);    
  }

  componentDidCatch(error, info) {
    // Display fallback UI
    //this.setState({ hasError: true });
    // You can also log the error to an error reporting service
    //logErrorToMyService(error, info);
    alert('We caught an error!');
  }

  // React "controlled components" approach... We control the DOM (and the text field), not the opposite.
  changeSearchText(evt) {
      this.setState({
          searchText: evt.target.value
      });
  }

  // Call backend with search query and start listening for updates.
  searchVacancies(evt) {
    evt.preventDefault();
    this.hubConnection.stop();
    var that = this;
    fetch(process.env.REACT_APP_BACKEND_API_URL + 'api/roomVacancy?query=' + this.state.searchText)
      .then((res) => {
        return res.json();
      })
      .catch(err => {
        console.log("Fetch error: " + err);
      })
      .then(function(json) {
        that.setState(
          {
            rooms: json
          }
        );
        that.hubConnection.start()
          .catch(err => console.error(err.toString()));
      });
  }

  // Recieve a vacancy update from the server (SignalR).
  vacancyUpdate(data) {

    // Create a temp state that is updated with any recieved items that are in our
    // result list.
    // Then do the proper React setting the new state (immutable).
    this.setState((state, props) => {
      data.forEach(x => {
        var found = state.rooms.find(elm => elm.id === x.id);
        if (found != null) {
          found.roomState = x.roomState
          found.updated = x.updated;
        }          
      });
      return ({
        rooms: state.rooms
      });
    });    
  }

  // Sub rendering of available display
  renderAvailable(available) {
    var cn = "text-warning";
    var txt = "Ukendt";
    if (available === 1) {
      cn = "text-success";
      txt = "Ledigt";
    }
    else if (available === 2) {
      cn = "text-danger";
      txt = "Optaget";
    }
      
    return (
      <td className={"text-uppercase " + cn}>{(txt)}</td>
    );
  }

  render() {
    return (
      <div className="App container mt-4 p-5" style={{ backgroundColor: '#00c000' }}>
        <div className="row justify-content-center" style={{ backgroundColor: '#ffffff' }}>
          <div className="col-10 p-3"><h3>Find et ledigt studielokale</h3></div>
        </div>
        <div className="row" style={{ backgroundColor: '#ffffff' }}>
          <div className="col-12 mt-5 mb-2">
            <form onSubmit={this.searchVacancies} >
              <div className="form-group input-group">
                  <input type="text" className="form-control"
                    placeholder="Bygning eller lokale nr." aria-label="Bygning eller lokale nr." aria-describedby="button-search"
                    value={this.state.searchText} onChange={this.changeSearchText} />
                  <div className="input-group-append">
                    <button type="submit" className="btn pl-5 pr-5" id="button-search">Søg</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
        <div className="row justify-content-center" style={{ backgroundColor: '#ffffff' }}>
          <div className="col-12 mt-3">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Bygning</th>
                  <th>Lokale</th>
                  <th>Status</th>
                  <th>Kort</th>
                </tr>
              </thead>
              <tbody>
                {this.state.rooms.map((room) =>
                  (
                    <tr key={room.id}>
                      <td>{room.building}</td>
                      <td>{room.room}</td>
                      {this.renderAvailable(room.roomState)}
                      <td>X</td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
