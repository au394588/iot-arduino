﻿namespace Au.Datalogger.Backend.Model
{
    public enum RoomType
    {
        Unknown = 0,
        MeetingRoom = 1
    }
}