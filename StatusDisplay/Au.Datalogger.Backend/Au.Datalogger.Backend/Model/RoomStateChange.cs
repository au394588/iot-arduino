﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Au.Datalogger.Backend.Model
{
    public enum RoomStateChange
    {
        New = 0,
        Changed = 1,
        Removed = 2
    }
}
