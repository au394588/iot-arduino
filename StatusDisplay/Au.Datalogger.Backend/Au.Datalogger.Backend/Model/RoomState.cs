﻿namespace Au.Datalogger.Backend.Model
{
    public enum RoomState
    {
        Unknown = 0,
        Free = 1,
        Occupied = 2
    }
}