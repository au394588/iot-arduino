﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Au.Datalogger.Backend.Model
{
    public class RoomVacancy
    {
        public int Id { get; set; }
        public DateTimeOffset Updated { get; set; }
        public string Building { get; set; }
        public string Room { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public RoomState RoomState { get; set; }
        public RoomType RoomType { get; set; }
    }
}
