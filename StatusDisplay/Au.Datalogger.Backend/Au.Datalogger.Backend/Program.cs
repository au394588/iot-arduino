﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;

namespace Au.Datalogger.Backend
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Setup serilog
            //   Reference: https://nblumhardt.com/2017/08/use-serilog/
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateLogger();

            try
            {
                Log.Information("Starting web host");

                // Build the web host.
                //
                // Errors will be caught and logged by the framework if Kestrel is behind IIS.
                //   Otherwise, our try/catch will catch it and log it.
                //   Reference: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/web-host?view=aspnetcore-2.1#capture-startup-errors
                //
                // Errors will be displayed on an error page if Kestrel is behind IIS, AND environment
                // is development.
                var host = WebHost.CreateDefaultBuilder(args)
                    .UseStartup<Startup>()
                    .UseSerilog()
                    .Build();

                host.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        // Read the configuration explicitly (this is usually done by the framework in
        // WebHost.CreateDefaultBuilder, but we need it earlier for configuring the logging.
        //   Reference: https://github.com/serilog/serilog-aspnetcore/tree/dev/samples/SimpleWebSample
        private static IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile(string.Format("appsettings.{0}.json", Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")), true, true)
            .AddEnvironmentVariables()
            .Build();
    }
}
